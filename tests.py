# -*- coding: utf-8 -*-
import base64
import unittest

from urlsafecrypt import InvalidToken
from urlsafecrypt.urlsafecrypt import _generate_token, _reappend_padding
from urlsafecrypt.urlsafecrypt import decrypt, encrypt


class UrlsafecryptTests(unittest.TestCase):

    def test_generate_token_from_unicode(self):
        token = _generate_token(u"mrrogers")
        token_dec = base64.b64decode(token)
        self.assertEqual(len(token_dec), 32)

    def test_generate_token_from_bytes(self):
        token = _generate_token(b"mrrogers")
        token_dec = base64.b64decode(token)
        self.assertEqual(len(token_dec), 32)

    def test_encrypt_decrypt_from_unicode(self):
        data = u"dätä"
        secret = u"sëcrët"
        data_enc = encrypt(data, secret)
        data_dec = decrypt(data_enc, secret)
        self.assertEqual(data, data_dec)

    def test_encrypt_decrypt_from_bytes(self):
        data = b"data"
        secret = b"secret"
        data_enc = encrypt(data, secret)
        data_dec = decrypt(data_enc, secret)
        self.assertEqual(data.decode('utf-8'), data_dec)

    def test_encrypt_and_decrypt_with_wrong_shared_secret(self):
        shared_secret = "mrrogers"
        data = "I'm interested in buying a hearing aid."

        data_enc = encrypt(data, shared_secret)
        with self.assertRaises(InvalidToken):
            decrypt(data_enc, "drwaring")

    def test_encrypt_strips_padding(self):
        shared_secret = "secret"
        data = "data"
        data_enc = encrypt(data, shared_secret)
        self.assertEqual(len(data_enc) % 4, 2)  # would have two fillchars
        self.assertNotEqual(data_enc[-1], b"=")  # must not end with padding

    def test_reappend_padding_1(self):
        encoded = b"Zm9vIGJhcg=="  # "foo bar"
        encoded_wo_padding = b"Zm9vIGJhcg"
        self.assertEqual(encoded, _reappend_padding(encoded_wo_padding))

    def test_reappend_padding_2(self):
        encoded = b"Zm9vIGJhciBiYXo="  # "foo bar baz"
        encoded_wo_padding = b"Zm9vIGJhciBiYXo"
        self.assertEqual(encoded, _reappend_padding(encoded_wo_padding))

    def test_reappend_padding_with_args(self):
        donald_trump = _reappend_padding(b':', multiple_of=2, fillchar=b'O')
        self.assertEqual(donald_trump, b':O')

    def test_import_encrypt_from_package(self):
        from urlsafecrypt import encrypt

    def test_import_decrypt_from_package(self):
        from urlsafecrypt import decrypt

    def test_import_invalidtoken_from_package(self):
        from urlsafecrypt import InvalidToken


if __name__ == '__main__':
    unittest.main()

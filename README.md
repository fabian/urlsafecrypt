# urlsafecrypt
Provides Fernet based symmetric encryption of data whose output is 
safe to be used in URLs.

To build the cryptography dependency on your system, please follow the 
[cryptography.io installation instructions](https://cryptography.io/en/latest/installation/).

```
import urlsafecrypt


shared_secret = "mrrogers"
data = "I'm interested in buying a hearing aid."

data_enc = urlsafecrypt.encrypt(data, shared_secret)
# -> b'gAAAAABXTrvIlJ3NFObDeo269ZiIse75UQZuFKtvrDx9QO3XuBHoZvhobe1jlhXs4RWJmeHWa9-DtBKrVx87JrbXzHSjqCvbSpZcsFjCdMlwtBQafcBuAnEuQxtAmv8Hk2_gS817Ntow'
data_dec = urlsafecrypt.decrypt(data_enc, shared_secret)
# -> "I'm interested in buying a hearing aid."

urlsafecrypt.decrypt(data_enc, "drwaring")
# -> raises InvalidToken
```
